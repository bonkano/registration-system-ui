import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { User } from './../models/User';
import { environment } from './../../environments/environment';

const PROTOCOL = 'http';
const PORT = 8080;
const API_SERVER_LOCATION = 'localhost';

@Injectable({
  providedIn: 'root'
})
export class UserService {
  baseUrl = 'http://localhost:8080/springboot-restserver/user/users';

  constructor(private http: HttpClient) {
    // this.baseUrl = `${PROTOCOL}://${API_SERVER_LOCATION}:${PORT}/springboot-restserver/user/`;
  }

  /**
   * Get all users as reference data from Backend server.
   */
  getUsers(): Observable<User[]> {
    let headers = new HttpHeaders();
    headers.append('content-type', 'application/json');
    headers.append('accept', 'application/json');
    return this.http.get<User[]>(this.baseUrl, { headers: headers });
  }
  /**
    * Save the given user object in the Backend server data base.
    * @param user
    */
  saveUser(user: User): Observable<User> {
    return this.http.post<User>(this.baseUrl, user);
  }
  /**
  * Update an existing user object in the Backend server data base.
  * @param user
  */
  updateUser(user: User): Observable<User> {
    return this.http.put<User>(this.baseUrl + 'users/updateUser', user);
  }

  /**
   * Delete an existing user object in the Backend server data base.
   * @param user
   */
  deleteUser(user: User): Observable<string> {
    return this.http.delete<string>(this.baseUrl + '/deleteUser/' + user.id);
  }
}
