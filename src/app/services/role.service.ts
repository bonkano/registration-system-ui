import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Role } from './../models/Role';

const PROTOCOL = 'htpp';
const PORT = 8080;

@Injectable({
  providedIn: 'root'
})
export class RoleService {
  baseUrl: string;

  constructor(private http: HttpClient) {
    this.baseUrl = `${PROTOCOL}://${location.hostname}:${PORT}/`;
  }

  /**
   * Get all roles as reference data from Backend server.
   */
  getRoles(): Observable<Role[]> {
    return this.http.get<Role[]>(this.baseUrl + 'roles');
  }
  /**
   * Save the given role in the Backend server data base
   */
  saveRole(role: Role): Observable<Role> {
    return this.http.post<Role>(this.baseUrl + 'roles/addRole', role);
  }
  /**
   * Update an existing role in the Backend server data base
   */
  updateRole(role: Role): Observable<Role> {
    return this.http.put<Role>(this.baseUrl + 'roles/updateRole', role);
  }

  /**
   * Delete an existing role object in the Backend server data base.
   * @param role
   */
  deleteRole(role: Role): Observable<string> {
    return this.http.delete<string>(this.baseUrl + '/deleteRole/' + role.id);
  }
}
