import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { User } from './../models/User';
import { BehaviorSubject, Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { environment } from './../../environments/environment';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class AuthenticationService {
  private currentUserSubject: BehaviorSubject<User>;
  public currentUser: Observable<User>;
  public isAuthenticated = new BehaviorSubject<boolean>(false);

  public authenticatedUser;
  public admin: boolean;
  public baseUrl: string;

  constructor(private http: HttpClient, private router: Router) {
    this.currentUserSubject = new BehaviorSubject<User>(JSON.parse(localStorage.getItem('currentUser')));
    this.currentUser = this.currentUserSubject.asObservable();
  }

  public get currentUserValue(): User {
    return this.currentUserSubject.value;
  }

  login(login: string, password: string) {
    return this.http.post<any>('http://localhost:8080/springboot-restserver/user/users/login', { login, password })
      .pipe(map(user => {
        // store user details and jwt token in local storage to keep user logged in between page refreshes
        localStorage.setItem('currentUser', JSON.stringify(user));
        this.isAuthenticated.next(true);
        this.currentUserSubject.next(user);
        return user;
      }));
  }

  logout(redirect: string) {
    // remove user from local storage to log user out
    try {
      localStorage.removeItem('currentUser');
      this.isAuthenticated.next(false);
      this.currentUserSubject.next(null);
      this.router.navigate([redirect]);
    } catch (error) {
      console.error(error);
    }
  }

  isAdmin(): boolean {
    let rolesNames: string[] = [];
    if (this.currentUser) {
      this.currentUserValue.roles.forEach(r => {
        rolesNames.push(r.roleName);
      });
    }
    return rolesNames.indexOf('ROLE_ADMIN') > -1;
  }

  loadUser() {
    return this.currentUserSubject.value;
  }
}
