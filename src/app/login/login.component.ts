import { Component, OnInit } from '@angular/core';
import { AuthenticationService } from '../services/authentication.service';
import { User } from '../models/User';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  login = '';
  password = '';
  return = '';

  constructor(public authService: AuthenticationService,
    private route: ActivatedRoute, private router: Router) { }

  ngOnInit(): void {
    this.route.queryParams.subscribe(params => {
      this.return = params.return;
      if (this.authService.isAuthenticated) {
        this.go();
      }
    });
  }

  signIn() {
    this.authService.login(this.login, this.password).subscribe(data => {
      console.log('User has successfully loged !');
      this.go();
    }, error => {
      console.log(error);
    });
  }

  /**
   * Method to call router to navigate back to return URL
   */
  go() {
    this.router.navigateByUrl(this.return);
    console.log(this.return);
  }
}
