import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { User } from './models/User';
import { AuthenticationService } from './services/authentication.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  currentUser: User;
  isAuthenticated: boolean;
  constructor(public authService: AuthenticationService) {
    this.authService.isAuthenticated.subscribe(
      (isAuthenticated: boolean) => this.isAuthenticated = isAuthenticated
    );
    // this.authService.currentUser.subscribe(x => this.currentUser = x);
  }
  ngOnInit(): void {
    this.authService.loadUser();
  }

  logout() {
    this.authService.logout('/login');
  }

  isAdmin() {
    return this.authService.isAdmin();
  }

}
