import { Role } from './Role';

export class User {
  id?: number;
  firstName: string;
  lastName: string;
  email: string;
  login?: string;
  password?: string;
  active?: boolean;
  roles?: Set<Role>;
  token?: string;
}
