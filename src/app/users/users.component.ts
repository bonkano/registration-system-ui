import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';
import { User } from './../models/User';
import { UserService } from './../services/user.service';
import { Observable } from 'rxjs';
import { AuthenticationService } from '../services/authentication.service';

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.css']
})
export class UsersComponent implements OnInit {
  displayedColumns: string[] = ['position', 'firstName', 'lastName', 'email', 'login'];
  dataSource = new MatTableDataSource();
  currentUser: User;
  users: User[] = [];

  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  constructor(public userService: UserService, private authService: AuthenticationService) {
    this.authService.currentUser.subscribe(x => this.currentUser = x);
  }

  ngOnInit(): void {
    this.userService.getUsers().subscribe(data => {
      this.dataSource.data = data;
      console.log('Users: ' + this.dataSource.data);
    }, error =>{
      console.log(error); // The error is here
    });
  }

}
